Platbamobilon sanbox
====================

Usage
-----

    $ git clone https://bitbucket.org/alhafoudh/platbamobilom-sandbox.git
    $ cd platbamobilom-sandbox/
    $ bundle install
    $ shotgun platbamobilom_sanbox.rb

    Visit http://localhost:9393/ for further instructions
