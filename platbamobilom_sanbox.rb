require 'sinatra'
require 'openssl'

require 'pry'

PASSWORD = "somekey"

class SMSPaymentRequest < Struct.new(:pid, :id, :desc, :price, :url, :email, :sign)

  def signature_data
    [pid, id, desc, price, url, email].join
  end

end

class SMSPaymentResponse < Struct.new(:id, :res, :phone, :sign)

  def signature_data
    [id, res, phone].join
  end

end

class PlatbamobilomSanbox
  get '/' do
    haml :index
  end

  get '/pay' do
    @price = params["PRICE"]
    haml :pay, format: :html5
  end

  post '/pay' do
    @price = params["PRICE"]
    haml :pay, format: :html5
  end

  post '/confirm' do
    payment_request = SMSPaymentRequest.new(params["PID"], params["ID"], params["DESC"], params["PRICE"], params["URL"], params["EMAIL"], params["SIGN"])
    payment_response = nil
    if params["confirm"] == "Confirm"    
      signature = sign(payment_request, PASSWORD)
      if signature == params["SIGN"]
        payment_response = SMSPaymentResponse.new(payment_request.id, "OK", "0909123456")
      else
        payment_response = SMSPaymentResponse.new(payment_request.id, "FAIL", "")
      end
    else
      payment_response = SMSPaymentResponse.new(payment_request.id, "FAIL", "")
    end
    payment_response.sign = sign(payment_response, PASSWORD)

    @url = callback_url(params["URL"], payment_response)    
    haml :confirm
  end

  get '/valid_sample' do
    redirect '/pay?PID=somepid&ID=someid&DESC=sample+payment&PRICE=0.20&URL=/debug&EMAIL=johndoe@example.com&SIGN=5B044CDF95A3599315F5EF337597753631877A1DD0048D39CFD32FDF64BC442D'
  end

  get '/invalid_sample' do
    redirect '/pay?PID=somepid&ID=someid&DESC=sample+payment&PRICE=0.20&URL=/debug&EMAIL=johndoe@example.com&SIGN=blabla'
  end

  get '/debug' do
    haml :debug
  end

  helpers do
    def sign(struct, pwd)
      digest = OpenSSL::Digest::Digest.new('sha256')
      hmac = OpenSSL::HMAC.digest(digest, pwd, struct.signature_data)
      signature = hmac.unpack("H*").join.upcase
    end

    def callback_url(url, response)
      "#{url}?&ID=#{response.id}&RES=#{response.res}&PHONE=#{response.phone}&SIGN=#{response.sign}"
    end
  end
end
